﻿Class Main

    Private Sub Start(sender As System.Object, e As System.EventArgs) Handles btnSort.Click, mnuBearbeitenSortieren.Click, tlbSort.Click

        '-----------------------------------------------------------------------
        ' Definition die Variabeln und den Array
        '-----------------------------------------------------------------------

        Dim strSort As String

        Dim douStart As Double

        Dim Wort() As String

        '-----------------------------------------------------------------------
        ' Löscht den schon vorhanden inhalt
        '-----------------------------------------------------------------------

        Me.txtAusgabe.Text = ""

        Me.txtTime.Text = ""

        '-----------------------------------------------------------------------
        ' Überprüft ob Text zum Sortieren vorhanden ist
        '-----------------------------------------------------------------------

        If txtEingabe.Text = "" Then

            MsgBox("Bitte geben sie ein Text ein zum Sortieren", MsgBoxStyle.OkOnly)

        Else

            strSort = Me.txtEingabe.Text

        End If

        '-----------------------------------------------------------------------
        ' Der Curser Wird in die Warte Position geschickt
        '-----------------------------------------------------------------------

        Me.Cursor = Cursors.WaitCursor

        '-----------------------------------------------------------------------
        ' Überprüft ob Umlaute Angeklickt ist 
        '-----------------------------------------------------------------------

        Dim boolUmlauteChecked As Boolean = Me.chkUmlaute.Checked

        '-----------------------------------------------------------------------
        ' Nimmt die Startzeit
        '-----------------------------------------------------------------------

        douStart = Microsoft.VisualBasic.Timer

        '-----------------------------------------------------------------------
        ' Überprüft welche Sortierart ausgewählt ist
        '-----------------------------------------------------------------------

        If Me.optZeichem.Checked = True Then

            '-----------------------------------------------------------------------
            ' Überprüft welcher Sortierer ausgewählt ist
            '-----------------------------------------------------------------------

            If Me.optQuick.Checked = True Then

                '-----------------------------------------------------------------------
                ' Übermittelt den String deren Lenge und ob Umlaute beachtet werden sollen
                '-----------------------------------------------------------------------

                QuickSort(strSort, 1, Len(strSort), boolUmlauteChecked)

            ElseIf Me.optBubble.Checked = True Then

                '-----------------------------------------------------------------------
                ' Übermittelt den String und ob Umlaute beachtet werden sollen
                '-----------------------------------------------------------------------

                BubbleSort(strSort, boolUmlauteChecked)

            ElseIf Me.optRippel.Checked = True Then

                '-----------------------------------------------------------------------
                ' Übermittelt den String deren Lenge und ob Umlaute beachtet werden sollen
                '-----------------------------------------------------------------------

                Rippelsort(strSort, 1, Len(strSort), boolUmlauteChecked)

            Else

                '-----------------------------------------------------------------------
                ' Erscheint bei nicht ausgewählten Sortierer
                '-----------------------------------------------------------------------

                MsgBox("Bitte Wählen sie einen Sortierer")

            End If

            '-----------------------------------------------------------------------
            ' Übermittelt den String und die Startzeit
            '-----------------------------------------------------------------------

            Ende(strSort, douStart)

        ElseIf Me.optWorten.Checked = True Then

            '-----------------------------------------------------------------------
            ' Übergibt den String der sie ins Array einfügt
            '-----------------------------------------------------------------------

            WortLesen(strSort, Wort)

            '-----------------------------------------------------------------------
            ' Überprüft ob Umlaute Aktiviert ist
            '-----------------------------------------------------------------------

            If boolUmlauteChecked = False Then

                If Me.optQuick.Checked = True Then

                    '-----------------------------------------------------------------------
                    ' Übermittelt den Wort Array deren Anfang und Ende
                    '-----------------------------------------------------------------------

                    QuickSortWort(Wort, LBound(Wort), UBound(Wort))

                ElseIf Me.optBubble.Checked = True Then

                    '-----------------------------------------------------------------------
                    ' Übermittelt den Wort Array
                    '-----------------------------------------------------------------------

                    BubbleSortWort(Wort)

                ElseIf Me.optRippel.Checked = True Then

                    '-----------------------------------------------------------------------
                    ' Übermittelt den Wort Array deren Anfang und Ende
                    '-----------------------------------------------------------------------

                    RippelsortWort(Wort, LBound(Wort), UBound(Wort))

                Else

                    '-----------------------------------------------------------------------
                    ' Erscheint bei nicht ausgewählten Sortierer
                    '-----------------------------------------------------------------------

                    MsgBox("Bitte Wählen sie einen Sortierer")

                End If

            Else

                If Me.optQuick.Checked = True Then

                    '-----------------------------------------------------------------------
                    ' Übermittelt den Wort Array deren Anfang und Ende
                    '-----------------------------------------------------------------------

                    QuickSortWortUmlaute(Wort, LBound(Wort), UBound(Wort))

                ElseIf Me.optBubble.Checked = True Then

                    '-----------------------------------------------------------------------
                    ' Übermittelt den Wort Array
                    '-----------------------------------------------------------------------

                    BubbleSortWortUmlaute(Wort)

                ElseIf Me.optRippel.Checked = True Then

                    '-----------------------------------------------------------------------
                    ' Übermittelt den Wort Array deren Anfang und Ende
                    '-----------------------------------------------------------------------

                    RippelsortWortUmlaute(Wort, LBound(Wort), UBound(Wort))

                Else

                    '-----------------------------------------------------------------------
                    ' Erscheint bei nicht ausgewählten Sortierer
                    '-----------------------------------------------------------------------

                    MsgBox("Bitte Wählen sie einen Sortierer")

                End If

            End If

            '-----------------------------------------------------------------------
            ' Übergibt den Array und wird wieder zusammengefüht
            '-----------------------------------------------------------------------

            WortSchreiben(strSort, Wort)

            '-----------------------------------------------------------------------
            ' Übermittelt den String und die Startzeit
            '-----------------------------------------------------------------------

            Ende(strSort, douStart)

        Else

            '-----------------------------------------------------------------------
            ' Erscheint bei nicht ausgewählten Sortierart
            '-----------------------------------------------------------------------

            MsgBox("Bitte Wählen sie einen Sortierart")

        End If

    End Sub

    Private Sub Ende(ByRef strSort As String, ByRef douStart As Double)

        '-----------------------------------------------------------------------
        ' Variabel des Wort Zählers 
        '-----------------------------------------------------------------------

        Dim strAnzahlWörter() As String

        '-----------------------------------------------------------------------
        ' Gibt die Zeit an die er fürs Sortieren gebraucht hat
        '-----------------------------------------------------------------------

        Me.txtTime.Text = Format(Microsoft.VisualBasic.Timer - douStart, "0.00000000000000000000s")

        Me.txtAusgabe.Text = strSort

        '-----------------------------------------------------------------------
        ' Ermittelt die Anzahl der Zeichen
        '-----------------------------------------------------------------------

        Me.txtLeng.Text = Len(strSort)

        '-----------------------------------------------------------------------
        ' Ermittelt die Anzahl Wörter beim Wort Sortierer
        '-----------------------------------------------------------------------

        If optWorten.Checked = True Then

            WortLesen(strSort, strAnzahlWörter)

            Me.txtWort.Text = UBound(strAnzahlWörter) + 1

        Else

            Me.txtWort.Text = "Keine Wörter"

        End If

        Me.Cursor = Cursors.Default

        '-----------------------------------------------------------------------
        ' Leert die Variabeln
        '-----------------------------------------------------------------------

        douStart = Nothing
        strSort = Nothing
        strAnzahlWörter = Nothing

    End Sub

    Private Sub btnBeend_Click(sender As System.Object, e As System.EventArgs) Handles btnBeend.Click, mnuDateiBeenden.Click

        '-----------------------------------------------------------------------
        ' Beendet das Programm beim Butten Beenden und im Menu Beenden
        '-----------------------------------------------------------------------

        Me.Close()

    End Sub

    Private Sub mnuInfos_Click(sender As System.Object, e As System.EventArgs) Handles mnuInfos.Click, tlbHilfe.Click

        '-----------------------------------------------------------------------
        ' Zeigt das Infofeld an
        '-----------------------------------------------------------------------

        modInfofeld.Show()

    End Sub

    Private Sub mnuBearbeitenAusschneiden_Click(sender As System.Object, e As System.EventArgs) Handles mnuBearbeitenAusschneiden.Click, tlbAusschneiden.Click

        '-----------------------------------------------------------------------
        ' Schneidet den Markirten Text aus
        '-----------------------------------------------------------------------

        If TypeOf Me.ActiveControl Is TextBox Then

            Clipboard.SetDataObject(CType(Me.ActiveControl, TextBox).SelectedText)
            Me.ActiveControl.Text = ""

        End If

    End Sub

    Private Sub mnuBearbeitenKopieren_Click(sender As System.Object, e As System.EventArgs) Handles mnuBearbeitenKopieren.Click, tlbKopieren.Click

        '-----------------------------------------------------------------------
        ' Kopiert den Markieren Text in den Zwischenspeicher
        '-----------------------------------------------------------------------
        Clipboard.SetDataObject(CType(Me.ActiveControl, TextBox).SelectedText)

    End Sub

    Private Sub mnuBearbeitenEinfügen_Click(sender As System.Object, e As System.EventArgs) Handles mnuBearbeitenEinfügen.Click, tlbEinfuegen.Click

        '-----------------------------------------------------------------------
        ' Fügt Text aus dem Zwischenspeicher in Aktive Feld
        '-----------------------------------------------------------------------

        CType(Me.ActiveControl, TextBox).SelectedText = Clipboard.GetDataObject.GetData(DataFormats.Text)

    End Sub

    Private Sub mnuOptionenFarbeFormular_Click(sender As System.Object, e As System.EventArgs) Handles mnuOptionenFarbeFormular.Click

        '-----------------------------------------------------------------------
        ' Ändert die Farbe des Formulars
        '-----------------------------------------------------------------------

        Dim dlgFarbe As New ColorDialog()
        dlgFarbe.Color = Me.BackColor
        dlgFarbe.ShowDialog()
        Me.BackColor = dlgFarbe.Color
        Me.tlbMenu.BackColor = dlgFarbe.Color
        Me.mnuMenu.BackColor = dlgFarbe.Color

    End Sub

    Private Sub mnuDateiNeu_Click(sender As System.Object, e As System.EventArgs) Handles mnuDateiNeu.Click, tlbNeu.Click

        '-----------------------------------------------------------------------
        ' Löscht den Ganzen Inhalt
        '-----------------------------------------------------------------------

        Me.txtAusgabe.Text = ""
        Me.txtEingabe.Text = ""
        Me.txtTime.Text = ""

    End Sub

    Private Sub mnuDateiSpeichern_Click(sender As System.Object, e As System.EventArgs) Handles mnuDateiSpeichern.Click, tlbSpeichern.Click

        '-----------------------------------------------------------------------
        ' Speichert den Markierten Text ab
        '-----------------------------------------------------------------------

        Dim intDateiHandle As Integer
        Dim strDateiInhalt As String
        Dim dlgSaveFile As New SaveFileDialog()

        If TypeOf ActiveControl Is TextBox Then

        Else

            MsgBox("Bitte Fokus in die zu speichernde Textbox setzen", MsgBoxStyle.Information)
            Exit Sub

        End If

        dlgSaveFile.Filter = "txt Dateien (*.txt)|*.txt|Alle Dateien (*.*)|*.*"
        dlgSaveFile.FilterIndex = 1
        dlgSaveFile.RestoreDirectory = True

        If dlgSaveFile.ShowDialog() = DialogResult.OK Then

            If Dir(dlgSaveFile.FileName) <> "" Then

                Kill(dlgSaveFile.FileName)

            End If

            intDateiHandle = FreeFile()
            FileOpen(intDateiHandle, dlgSaveFile.FileName, OpenMode.Binary)
            strDateiInhalt = ActiveControl.Text
            FilePut(intDateiHandle, strDateiInhalt, 1)
            FileClose(intDateiHandle)

        End If

    End Sub

    Private Sub mnuDateiÖffnen_Click(sender As System.Object, e As System.EventArgs) Handles mnuDateiÖffnen.Click, tlbOeffnen.Click
        Dim dlgOpenfile As New OpenFileDialog()

        dlgOpenfile.Filter = "txt Dateien (*.txt)|*.txt|Alle Datien (*.*)|*.*"
        dlgOpenfile.FilterIndex = 1
        dlgOpenfile.RestoreDirectory = True
        If dlgOpenfile.ShowDialog() = DialogResult.OK Then
            Dim sr As New System.IO.StreamReader(dlgOpenfile.FileName)
            Me.txtEingabe.Text = sr.ReadToEnd
            sr.Close()
        End If
    End Sub

    Private Sub mnuBearbeiten_DropDownOpening(sender As Object, e As System.EventArgs) Handles mnuBearbeiten.DropDownOpening, tlbMenu.MouseHover

        '-----------------------------------------------------------------------
        ' Aktivirt die Bearbeitungs Menü
        '-----------------------------------------------------------------------

        If Clipboard.GetDataObject.GetData(DataFormats.Text) <> "" Then

            Me.mnuBearbeitenEinfügen.Enabled = True
            Me.tlbEinfuegen.Enabled = True

        Else

            Me.mnuBearbeitenEinfügen.Enabled = False
            Me.tlbEinfuegen.Enabled = False

        End If

        If TypeOf Me.ActiveControl Is TextBox Then

            If CType(Me.ActiveControl, TextBox).SelectedText <> "" Then

                Me.mnuBearbeitenAusschneiden.Enabled = True
                Me.mnuBearbeitenKopieren.Enabled = True
                Me.tlbAusschneiden.Enabled = True
                Me.tlbKopieren.Enabled = True

            Else

                Me.mnuBearbeitenAusschneiden.Enabled = False
                Me.mnuBearbeitenKopieren.Enabled = False
                Me.tlbAusschneiden.Enabled = False
                Me.tlbKopieren.Enabled = False

            End If

        End If

    End Sub

    Private Sub mnuOptionenFarbeTextbox_Click(sender As System.Object, e As System.EventArgs) Handles mnuOptionenFarbeTextbox.Click

        '-----------------------------------------------------------------------
        ' Ändert die Farben der TextBoxen
        '-----------------------------------------------------------------------

        Dim dlgFarbe As New ColorDialog()
        dlgFarbe.Color = Me.txtEingabe.BackColor
        dlgFarbe.Color = Me.txtAusgabe.BackColor

        dlgFarbe.ShowDialog()
        Me.txtEingabe.BackColor = dlgFarbe.Color
        Me.txtAusgabe.BackColor = dlgFarbe.Color

    End Sub

    Private Sub mnuOptionenUmlaute_Click(sender As System.Object, e As System.EventArgs) Handles mnuOptionenUmlaute.Click, chkUmlaute.Click

        '-----------------------------------------------------------------------
        ' Aktiviert und Deaktivirt die Umlautbox
        '-----------------------------------------------------------------------

        If Me.mnuOptionenUmlaute.Checked = False Then

            Me.mnuOptionenUmlaute.Checked = True
            Me.chkUmlaute.Checked = True

        Else

            Me.mnuOptionenUmlaute.Checked = False
            Me.chkUmlaute.Checked = False

        End If
    End Sub

    Private Sub btnBeend_MouseWheel(sender As Object, e As System.Windows.Forms.MouseEventArgs) Handles btnBeend.MouseHover

        Me.Cursor = Cursors.Default

    End Sub

End Class


