﻿Module modWortReplaceJoin

    '-----------------------------------------------------------------------
    ' Alle unötigen Zeichen werdeen ersetzt und Wort für Wort in das Array gespeichert
    '-----------------------------------------------------------------------

    Public Sub WortLesen(ByRef strSort As String, ByRef Wort() As String)

        '-----------------------------------------------------------------------
        ' Ersetzen der Zeichen
        '-----------------------------------------------------------------------

        strSort = Replace(strSort, vbCrLf, " ")
        strSort = Replace(strSort, vbTab, " ")
        strSort = Replace(strSort, ",", " ")
        strSort = Replace(strSort, ".", " ")

        While InStr(strSort, "  ") > 0
            strSort = Replace(strSort, "  ", " ")
        End While

        '-----------------------------------------------------------------------
        ' Worte werdn ins Array Wort gespeichert
        '-----------------------------------------------------------------------

        Wort = Split(Trim(strSort), " ")

    End Sub

    Public Sub WortSchreiben(ByRef strSort As String, ByRef Wort() As String)

        '-----------------------------------------------------------------------
        ' Die Einzelnen Wörter im Array werden zusammengeführt
        '-----------------------------------------------------------------------

        strSort = Join(Wort, " ")

    End Sub

End Module
