﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Main
    Inherits System.Windows.Forms.Form

    'Das Formular überschreibt den Löschvorgang, um die Komponentenliste zu bereinigen.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Wird vom Windows Form-Designer benötigt.
    Private components As System.ComponentModel.IContainer

    'Hinweis: Die folgende Prozedur ist für den Windows Form-Designer erforderlich.
    'Das Bearbeiten ist mit dem Windows Form-Designer möglich.  
    'Das Bearbeiten mit dem Code-Editor ist nicht möglich.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Main))
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.grpUnsort = New System.Windows.Forms.GroupBox()
        Me.txtEingabe = New System.Windows.Forms.TextBox()
        Me.mnuBearbeitenEinfügen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBearbeitenSortieren = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBearbeitenKopieren = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDateiBeenden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBearbeitenAusschneiden = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionenUmlaute = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuOptionenFarbe = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionenFarbeFormular = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuOptionenFarbeTextbox = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuBearbeiten = New System.Windows.Forms.ToolStripMenuItem()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.optWorten = New System.Windows.Forms.RadioButton()
        Me.optZeichem = New System.Windows.Forms.RadioButton()
        Me.mnuInfos = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator2 = New System.Windows.Forms.ToolStripSeparator()
        Me.mnuDateiSpeichern = New System.Windows.Forms.ToolStripMenuItem()
        Me.txtTime = New System.Windows.Forms.Label()
        Me.optRippel = New System.Windows.Forms.RadioButton()
        Me.chkUmlaute = New System.Windows.Forms.CheckBox()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.btnBeend = New System.Windows.Forms.Button()
        Me.lblSortierd = New System.Windows.Forms.Label()
        Me.optQuick = New System.Windows.Forms.RadioButton()
        Me.txtAusgabe = New System.Windows.Forms.TextBox()
        Me.optBubble = New System.Windows.Forms.RadioButton()
        Me.mnuDateiÖffnen = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDateiNeu = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnSort = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.mnuDatei = New System.Windows.Forms.ToolStripMenuItem()
        Me.grpSort = New System.Windows.Forms.GroupBox()
        Me.mnuMenu = New System.Windows.Forms.MenuStrip()
        Me.tlbMenu = New System.Windows.Forms.ToolStrip()
        Me.tlbNeu = New System.Windows.Forms.ToolStripButton()
        Me.tlbOeffnen = New System.Windows.Forms.ToolStripButton()
        Me.tlbSpeichern = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.tlbAusschneiden = New System.Windows.Forms.ToolStripButton()
        Me.tlbKopieren = New System.Windows.Forms.ToolStripButton()
        Me.tlbEinfuegen = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.tlbSort = New System.Windows.Forms.ToolStripButton()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.tlbHilfe = New System.Windows.Forms.ToolStripButton()
        Me.txtLeng = New System.Windows.Forms.Label()
        Me.lblZeichen = New System.Windows.Forms.Label()
        Me.txtWort = New System.Windows.Forms.Label()
        Me.lblWörter = New System.Windows.Forms.Label()
        Me.grpUnsort.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.grpSort.SuspendLayout()
        Me.mnuMenu.SuspendLayout()
        Me.tlbMenu.SuspendLayout()
        Me.SuspendLayout()
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(188, 6)
        '
        'grpUnsort
        '
        Me.grpUnsort.Controls.Add(Me.txtEingabe)
        Me.grpUnsort.Location = New System.Drawing.Point(12, 47)
        Me.grpUnsort.Name = "grpUnsort"
        Me.grpUnsort.Size = New System.Drawing.Size(650, 279)
        Me.grpUnsort.TabIndex = 8
        Me.grpUnsort.TabStop = False
        Me.grpUnsort.Text = "Unsortiert"
        '
        'txtEingabe
        '
        Me.txtEingabe.Location = New System.Drawing.Point(9, 15)
        Me.txtEingabe.Multiline = True
        Me.txtEingabe.Name = "txtEingabe"
        Me.txtEingabe.Size = New System.Drawing.Size(630, 253)
        Me.txtEingabe.TabIndex = 0
        '
        'mnuBearbeitenEinfügen
        '
        Me.mnuBearbeitenEinfügen.Image = Global.Sortierer.My.Resources.Resources.cllipboard_32
        Me.mnuBearbeitenEinfügen.Name = "mnuBearbeitenEinfügen"
        Me.mnuBearbeitenEinfügen.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.mnuBearbeitenEinfügen.Size = New System.Drawing.Size(191, 22)
        Me.mnuBearbeitenEinfügen.Text = "Einfügen"
        '
        'mnuBearbeitenSortieren
        '
        Me.mnuBearbeitenSortieren.Image = Global.Sortierer.My.Resources.Resources.alphabetical_sorting2_32
        Me.mnuBearbeitenSortieren.Name = "mnuBearbeitenSortieren"
        Me.mnuBearbeitenSortieren.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuBearbeitenSortieren.Size = New System.Drawing.Size(191, 22)
        Me.mnuBearbeitenSortieren.Text = "Sortieren"
        '
        'mnuBearbeitenKopieren
        '
        Me.mnuBearbeitenKopieren.Image = Global.Sortierer.My.Resources.Resources.copy_26
        Me.mnuBearbeitenKopieren.Name = "mnuBearbeitenKopieren"
        Me.mnuBearbeitenKopieren.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.mnuBearbeitenKopieren.Size = New System.Drawing.Size(191, 22)
        Me.mnuBearbeitenKopieren.Text = "Kopieren"
        '
        'mnuDateiBeenden
        '
        Me.mnuDateiBeenden.Image = Global.Sortierer.My.Resources.Resources.close_window_32
        Me.mnuDateiBeenden.Name = "mnuDateiBeenden"
        Me.mnuDateiBeenden.ShortcutKeys = CType((System.Windows.Forms.Keys.Alt Or System.Windows.Forms.Keys.F4), System.Windows.Forms.Keys)
        Me.mnuDateiBeenden.Size = New System.Drawing.Size(168, 22)
        Me.mnuDateiBeenden.Text = "Beenden"
        '
        'mnuBearbeitenAusschneiden
        '
        Me.mnuBearbeitenAusschneiden.Image = Global.Sortierer.My.Resources.Resources.cut_26
        Me.mnuBearbeitenAusschneiden.Name = "mnuBearbeitenAusschneiden"
        Me.mnuBearbeitenAusschneiden.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.mnuBearbeitenAusschneiden.Size = New System.Drawing.Size(191, 22)
        Me.mnuBearbeitenAusschneiden.Text = "Ausschneiden"
        '
        'mnuOptionen
        '
        Me.mnuOptionen.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuOptionenUmlaute, Me.ToolStripSeparator4, Me.mnuOptionenFarbe})
        Me.mnuOptionen.Name = "mnuOptionen"
        Me.mnuOptionen.Size = New System.Drawing.Size(69, 20)
        Me.mnuOptionen.Text = "Optionen"
        '
        'mnuOptionenUmlaute
        '
        Me.mnuOptionenUmlaute.Name = "mnuOptionenUmlaute"
        Me.mnuOptionenUmlaute.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.U), System.Windows.Forms.Keys)
        Me.mnuOptionenUmlaute.Size = New System.Drawing.Size(163, 22)
        Me.mnuOptionenUmlaute.Text = "Umlaute"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(160, 6)
        '
        'mnuOptionenFarbe
        '
        Me.mnuOptionenFarbe.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuOptionenFarbeFormular, Me.mnuOptionenFarbeTextbox})
        Me.mnuOptionenFarbe.Name = "mnuOptionenFarbe"
        Me.mnuOptionenFarbe.Size = New System.Drawing.Size(163, 22)
        Me.mnuOptionenFarbe.Text = "Farbe"
        '
        'mnuOptionenFarbeFormular
        '
        Me.mnuOptionenFarbeFormular.Name = "mnuOptionenFarbeFormular"
        Me.mnuOptionenFarbeFormular.Size = New System.Drawing.Size(122, 22)
        Me.mnuOptionenFarbeFormular.Text = "Formular"
        '
        'mnuOptionenFarbeTextbox
        '
        Me.mnuOptionenFarbeTextbox.Name = "mnuOptionenFarbeTextbox"
        Me.mnuOptionenFarbeTextbox.Size = New System.Drawing.Size(122, 22)
        Me.mnuOptionenFarbeTextbox.Text = "Textbox"
        '
        'mnuBearbeiten
        '
        Me.mnuBearbeiten.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuBearbeitenAusschneiden, Me.mnuBearbeitenKopieren, Me.mnuBearbeitenEinfügen, Me.ToolStripSeparator3, Me.mnuBearbeitenSortieren})
        Me.mnuBearbeiten.Name = "mnuBearbeiten"
        Me.mnuBearbeiten.Size = New System.Drawing.Size(75, 20)
        Me.mnuBearbeiten.Text = "Bearbeiten"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.optWorten)
        Me.GroupBox1.Controls.Add(Me.optZeichem)
        Me.GroupBox1.Location = New System.Drawing.Point(687, 167)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(131, 72)
        Me.GroupBox1.TabIndex = 13
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Sortierart"
        '
        'optWorten
        '
        Me.optWorten.AutoSize = True
        Me.optWorten.Location = New System.Drawing.Point(7, 43)
        Me.optWorten.Name = "optWorten"
        Me.optWorten.Size = New System.Drawing.Size(89, 17)
        Me.optWorten.TabIndex = 1
        Me.optWorten.TabStop = True
        Me.optWorten.Text = "Nach Worten"
        Me.optWorten.UseVisualStyleBackColor = True
        '
        'optZeichem
        '
        Me.optZeichem.AutoSize = True
        Me.optZeichem.Location = New System.Drawing.Point(7, 20)
        Me.optZeichem.Name = "optZeichem"
        Me.optZeichem.Size = New System.Drawing.Size(93, 17)
        Me.optZeichem.TabIndex = 0
        Me.optZeichem.TabStop = True
        Me.optZeichem.Text = "Nach Zeichen"
        Me.optZeichem.UseVisualStyleBackColor = True
        '
        'mnuInfos
        '
        Me.mnuInfos.Name = "mnuInfos"
        Me.mnuInfos.ShortcutKeys = System.Windows.Forms.Keys.F1
        Me.mnuInfos.Size = New System.Drawing.Size(45, 20)
        Me.mnuInfos.Text = "Infos"
        '
        'ToolStripSeparator2
        '
        Me.ToolStripSeparator2.Name = "ToolStripSeparator2"
        Me.ToolStripSeparator2.Size = New System.Drawing.Size(165, 6)
        '
        'mnuDateiSpeichern
        '
        Me.mnuDateiSpeichern.Image = Global.Sortierer.My.Resources.Resources.save_as_32
        Me.mnuDateiSpeichern.Name = "mnuDateiSpeichern"
        Me.mnuDateiSpeichern.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.mnuDateiSpeichern.Size = New System.Drawing.Size(168, 22)
        Me.mnuDateiSpeichern.Text = "Speichern"
        '
        'txtTime
        '
        Me.txtTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtTime.Location = New System.Drawing.Point(684, 429)
        Me.txtTime.Name = "txtTime"
        Me.txtTime.Size = New System.Drawing.Size(124, 20)
        Me.txtTime.TabIndex = 14
        '
        'optRippel
        '
        Me.optRippel.AutoSize = True
        Me.optRippel.Location = New System.Drawing.Point(6, 66)
        Me.optRippel.Name = "optRippel"
        Me.optRippel.Size = New System.Drawing.Size(77, 17)
        Me.optRippel.TabIndex = 3
        Me.optRippel.TabStop = True
        Me.optRippel.Text = "Rippel-Sort"
        Me.optRippel.UseVisualStyleBackColor = True
        '
        'chkUmlaute
        '
        Me.chkUmlaute.AutoSize = True
        Me.chkUmlaute.Location = New System.Drawing.Point(7, 93)
        Me.chkUmlaute.Name = "chkUmlaute"
        Me.chkUmlaute.Size = New System.Drawing.Size(65, 17)
        Me.chkUmlaute.TabIndex = 2
        Me.chkUmlaute.Text = "Umlaute"
        Me.chkUmlaute.UseVisualStyleBackColor = True
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(165, 6)
        '
        'btnBeend
        '
        Me.btnBeend.Location = New System.Drawing.Point(687, 328)
        Me.btnBeend.Name = "btnBeend"
        Me.btnBeend.Size = New System.Drawing.Size(131, 23)
        Me.btnBeend.TabIndex = 12
        Me.btnBeend.Text = "Beenden"
        Me.btnBeend.UseVisualStyleBackColor = True
        '
        'lblSortierd
        '
        Me.lblSortierd.AutoSize = True
        Me.lblSortierd.Location = New System.Drawing.Point(684, 410)
        Me.lblSortierd.Name = "lblSortierd"
        Me.lblSortierd.Size = New System.Drawing.Size(67, 13)
        Me.lblSortierd.TabIndex = 15
        Me.lblSortierd.Text = "Sortierdauer:"
        '
        'optQuick
        '
        Me.optQuick.AutoSize = True
        Me.optQuick.Location = New System.Drawing.Point(7, 43)
        Me.optQuick.Name = "optQuick"
        Me.optQuick.Size = New System.Drawing.Size(75, 17)
        Me.optQuick.TabIndex = 1
        Me.optQuick.TabStop = True
        Me.optQuick.Text = "Quick-Sort"
        Me.optQuick.UseVisualStyleBackColor = True
        '
        'txtAusgabe
        '
        Me.txtAusgabe.Location = New System.Drawing.Point(10, 16)
        Me.txtAusgabe.Multiline = True
        Me.txtAusgabe.Name = "txtAusgabe"
        Me.txtAusgabe.Size = New System.Drawing.Size(630, 253)
        Me.txtAusgabe.TabIndex = 1
        '
        'optBubble
        '
        Me.optBubble.AutoSize = True
        Me.optBubble.Location = New System.Drawing.Point(7, 20)
        Me.optBubble.Name = "optBubble"
        Me.optBubble.Size = New System.Drawing.Size(80, 17)
        Me.optBubble.TabIndex = 0
        Me.optBubble.TabStop = True
        Me.optBubble.Text = "Bubble-Sort"
        Me.optBubble.UseVisualStyleBackColor = True
        '
        'mnuDateiÖffnen
        '
        Me.mnuDateiÖffnen.Image = Global.Sortierer.My.Resources.Resources.folder_32
        Me.mnuDateiÖffnen.Name = "mnuDateiÖffnen"
        Me.mnuDateiÖffnen.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.mnuDateiÖffnen.Size = New System.Drawing.Size(168, 22)
        Me.mnuDateiÖffnen.Text = "Öffnen"
        '
        'mnuDateiNeu
        '
        Me.mnuDateiNeu.Image = Global.Sortierer.My.Resources.Resources.document_32
        Me.mnuDateiNeu.Name = "mnuDateiNeu"
        Me.mnuDateiNeu.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.mnuDateiNeu.Size = New System.Drawing.Size(168, 22)
        Me.mnuDateiNeu.Text = "Neu"
        '
        'btnSort
        '
        Me.btnSort.Location = New System.Drawing.Point(687, 289)
        Me.btnSort.Name = "btnSort"
        Me.btnSort.Size = New System.Drawing.Size(131, 23)
        Me.btnSort.TabIndex = 11
        Me.btnSort.Text = "Sortieren"
        Me.btnSort.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.optRippel)
        Me.GroupBox3.Controls.Add(Me.chkUmlaute)
        Me.GroupBox3.Controls.Add(Me.optQuick)
        Me.GroupBox3.Controls.Add(Me.optBubble)
        Me.GroupBox3.Location = New System.Drawing.Point(687, 48)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(131, 116)
        Me.GroupBox3.TabIndex = 10
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Sortierer"
        '
        'mnuDatei
        '
        Me.mnuDatei.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDateiNeu, Me.mnuDateiÖffnen, Me.ToolStripSeparator1, Me.mnuDateiSpeichern, Me.ToolStripSeparator2, Me.mnuDateiBeenden})
        Me.mnuDatei.Name = "mnuDatei"
        Me.mnuDatei.Size = New System.Drawing.Size(46, 20)
        Me.mnuDatei.Text = "Datei"
        '
        'grpSort
        '
        Me.grpSort.Controls.Add(Me.txtAusgabe)
        Me.grpSort.Location = New System.Drawing.Point(12, 326)
        Me.grpSort.Name = "grpSort"
        Me.grpSort.Size = New System.Drawing.Size(651, 279)
        Me.grpSort.TabIndex = 9
        Me.grpSort.TabStop = False
        Me.grpSort.Text = "Sortiert"
        '
        'mnuMenu
        '
        Me.mnuMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuDatei, Me.mnuBearbeiten, Me.mnuOptionen, Me.mnuInfos})
        Me.mnuMenu.Location = New System.Drawing.Point(0, 0)
        Me.mnuMenu.Name = "mnuMenu"
        Me.mnuMenu.Size = New System.Drawing.Size(834, 24)
        Me.mnuMenu.TabIndex = 16
        '
        'tlbMenu
        '
        Me.tlbMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tlbNeu, Me.tlbOeffnen, Me.tlbSpeichern, Me.ToolStripSeparator5, Me.tlbAusschneiden, Me.tlbKopieren, Me.tlbEinfuegen, Me.ToolStripSeparator6, Me.tlbSort, Me.ToolStripSeparator7, Me.tlbHilfe})
        Me.tlbMenu.Location = New System.Drawing.Point(0, 24)
        Me.tlbMenu.Name = "tlbMenu"
        Me.tlbMenu.Size = New System.Drawing.Size(834, 25)
        Me.tlbMenu.TabIndex = 17
        Me.tlbMenu.Text = "Menu_Kurz"
        '
        'tlbNeu
        '
        Me.tlbNeu.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tlbNeu.Image = Global.Sortierer.My.Resources.Resources.document_32
        Me.tlbNeu.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbNeu.Name = "tlbNeu"
        Me.tlbNeu.Size = New System.Drawing.Size(23, 22)
        Me.tlbNeu.Text = "Neu"
        '
        'tlbOeffnen
        '
        Me.tlbOeffnen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tlbOeffnen.Image = Global.Sortierer.My.Resources.Resources.folder_32
        Me.tlbOeffnen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbOeffnen.Name = "tlbOeffnen"
        Me.tlbOeffnen.Size = New System.Drawing.Size(23, 22)
        Me.tlbOeffnen.Text = "Öffnen"
        '
        'tlbSpeichern
        '
        Me.tlbSpeichern.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tlbSpeichern.Image = Global.Sortierer.My.Resources.Resources.save_as_32
        Me.tlbSpeichern.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbSpeichern.Name = "tlbSpeichern"
        Me.tlbSpeichern.Size = New System.Drawing.Size(23, 22)
        Me.tlbSpeichern.Text = "Speichern"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(6, 25)
        '
        'tlbAusschneiden
        '
        Me.tlbAusschneiden.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tlbAusschneiden.Image = Global.Sortierer.My.Resources.Resources.cut_26
        Me.tlbAusschneiden.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbAusschneiden.Name = "tlbAusschneiden"
        Me.tlbAusschneiden.Size = New System.Drawing.Size(23, 22)
        Me.tlbAusschneiden.Text = "Ausschneiden"
        '
        'tlbKopieren
        '
        Me.tlbKopieren.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tlbKopieren.Image = Global.Sortierer.My.Resources.Resources.copy_26
        Me.tlbKopieren.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbKopieren.Name = "tlbKopieren"
        Me.tlbKopieren.Size = New System.Drawing.Size(23, 22)
        Me.tlbKopieren.Text = "Kopieren"
        '
        'tlbEinfuegen
        '
        Me.tlbEinfuegen.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tlbEinfuegen.Image = Global.Sortierer.My.Resources.Resources.cllipboard_32
        Me.tlbEinfuegen.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbEinfuegen.Name = "tlbEinfuegen"
        Me.tlbEinfuegen.Size = New System.Drawing.Size(23, 22)
        Me.tlbEinfuegen.Text = "Einfügen"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(6, 25)
        '
        'tlbSort
        '
        Me.tlbSort.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tlbSort.Image = Global.Sortierer.My.Resources.Resources.alphabetical_sorting2_32
        Me.tlbSort.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbSort.Name = "tlbSort"
        Me.tlbSort.Size = New System.Drawing.Size(23, 22)
        Me.tlbSort.Text = "Sortieren"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(6, 25)
        '
        'tlbHilfe
        '
        Me.tlbHilfe.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tlbHilfe.Image = Global.Sortierer.My.Resources.Resources.help_32
        Me.tlbHilfe.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tlbHilfe.Name = "tlbHilfe"
        Me.tlbHilfe.Size = New System.Drawing.Size(23, 22)
        Me.tlbHilfe.Text = "Hilfe"
        '
        'txtLeng
        '
        Me.txtLeng.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtLeng.Location = New System.Drawing.Point(684, 475)
        Me.txtLeng.Name = "txtLeng"
        Me.txtLeng.Size = New System.Drawing.Size(124, 20)
        Me.txtLeng.TabIndex = 18
        '
        'lblZeichen
        '
        Me.lblZeichen.AutoSize = True
        Me.lblZeichen.Location = New System.Drawing.Point(684, 456)
        Me.lblZeichen.Name = "lblZeichen"
        Me.lblZeichen.Size = New System.Drawing.Size(84, 13)
        Me.lblZeichen.TabIndex = 19
        Me.lblZeichen.Text = "Anzahl Zeichen:"
        '
        'txtWort
        '
        Me.txtWort.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtWort.Enabled = False
        Me.txtWort.Location = New System.Drawing.Point(684, 524)
        Me.txtWort.Name = "txtWort"
        Me.txtWort.Size = New System.Drawing.Size(124, 20)
        Me.txtWort.TabIndex = 20
        '
        'lblWörter
        '
        Me.lblWörter.AutoSize = True
        Me.lblWörter.Location = New System.Drawing.Point(684, 505)
        Me.lblWörter.Name = "lblWörter"
        Me.lblWörter.Size = New System.Drawing.Size(77, 13)
        Me.lblWörter.TabIndex = 21
        Me.lblWörter.Text = "Anzahl Wörter:"
        '
        'Main
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(834, 609)
        Me.Controls.Add(Me.txtWort)
        Me.Controls.Add(Me.lblWörter)
        Me.Controls.Add(Me.txtLeng)
        Me.Controls.Add(Me.lblZeichen)
        Me.Controls.Add(Me.tlbMenu)
        Me.Controls.Add(Me.grpUnsort)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txtTime)
        Me.Controls.Add(Me.btnBeend)
        Me.Controls.Add(Me.lblSortierd)
        Me.Controls.Add(Me.btnSort)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.grpSort)
        Me.Controls.Add(Me.mnuMenu)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MaximumSize = New System.Drawing.Size(850, 647)
        Me.MinimizeBox = False
        Me.MinimumSize = New System.Drawing.Size(850, 647)
        Me.Name = "Main"
        Me.Text = "Sortierer"
        Me.grpUnsort.ResumeLayout(False)
        Me.grpUnsort.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.grpSort.ResumeLayout(False)
        Me.grpSort.PerformLayout()
        Me.mnuMenu.ResumeLayout(False)
        Me.mnuMenu.PerformLayout()
        Me.tlbMenu.ResumeLayout(False)
        Me.tlbMenu.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolStripSeparator3 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents grpUnsort As System.Windows.Forms.GroupBox
    Friend WithEvents txtEingabe As System.Windows.Forms.TextBox
    Friend WithEvents mnuBearbeitenEinfügen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBearbeitenSortieren As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBearbeitenKopieren As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDateiBeenden As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBearbeitenAusschneiden As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOptionen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOptionenUmlaute As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuOptionenFarbe As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOptionenFarbeFormular As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuOptionenFarbeTextbox As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuBearbeiten As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents optWorten As System.Windows.Forms.RadioButton
    Friend WithEvents optZeichem As System.Windows.Forms.RadioButton
    Friend WithEvents mnuInfos As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator2 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents mnuDateiSpeichern As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents txtTime As System.Windows.Forms.Label
    Friend WithEvents optRippel As System.Windows.Forms.RadioButton
    Friend WithEvents chkUmlaute As System.Windows.Forms.CheckBox
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents btnBeend As System.Windows.Forms.Button
    Friend WithEvents lblSortierd As System.Windows.Forms.Label
    Friend WithEvents optQuick As System.Windows.Forms.RadioButton
    Friend WithEvents txtAusgabe As System.Windows.Forms.TextBox
    Friend WithEvents optBubble As System.Windows.Forms.RadioButton
    Friend WithEvents mnuDateiÖffnen As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDateiNeu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnSort As System.Windows.Forms.Button
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents mnuDatei As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents grpSort As System.Windows.Forms.GroupBox
    Friend WithEvents mnuMenu As System.Windows.Forms.MenuStrip
    Friend WithEvents tlbMenu As System.Windows.Forms.ToolStrip
    Friend WithEvents tlbNeu As System.Windows.Forms.ToolStripButton
    Friend WithEvents tlbOeffnen As System.Windows.Forms.ToolStripButton
    Friend WithEvents tlbSpeichern As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator5 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tlbAusschneiden As System.Windows.Forms.ToolStripButton
    Friend WithEvents tlbKopieren As System.Windows.Forms.ToolStripButton
    Friend WithEvents tlbEinfuegen As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator6 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tlbSort As System.Windows.Forms.ToolStripButton
    Friend WithEvents ToolStripSeparator7 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tlbHilfe As System.Windows.Forms.ToolStripButton
    Friend WithEvents txtLeng As System.Windows.Forms.Label
    Friend WithEvents lblZeichen As System.Windows.Forms.Label
    Friend WithEvents txtWort As System.Windows.Forms.Label
    Friend WithEvents lblWörter As System.Windows.Forms.Label

End Class
