﻿Option Compare Text
Module modSortierenWortUmlate

    '-----------------------------------------------------------------------
    ' QuickSortWortUmlaute Funktion für Wörter 
    ' Erklärung http://de.wikipedia.org/wiki/Quicksort
    '-----------------------------------------------------------------------

    Function QuickSortWortUmlaute(ByRef Wort() As String, ByVal Anfang As Integer, ByVal Ende As Integer)

        Dim i As Integer = Anfang
        Dim j As Integer = Ende
        Dim X As String = Wort(Math.Round((Anfang + Ende) / 2))
        Dim y As String

        Do

            While Wort(i) < X
                i = i + 1
            End While

            While X < Wort(j)
                j = j - 1
            End While

            If i <= j Then
                y = Wort(i)
                Wort(i) = Wort(j)
                Wort(j) = y
                i = i + 1
                j = j - 1
            End If

        Loop Until i > j

        If Anfang < j Then
            QuickSortWort(Wort, Anfang, j)
        End If

        If i < Ende Then
            QuickSortWort(Wort, i, Ende)
        End If

        Return Wort

    End Function

    '-----------------------------------------------------------------------
    ' BubbelesortWortUmlaute Funktion für Zeichen
    ' Erklärung http://de.wikipedia.org/wiki/Bubblesort
    '-----------------------------------------------------------------------

    Function BubbleSortWortUmlaute(ByRef Wort() As String)

        Dim strWort As String
        Dim blogetauscht As Boolean

        Do

            blogetauscht = False

            For i = LBound(Wort) To UBound(Wort) - 1

                If Wort(i) > Wort(i + 1) Then
                    strWort = Wort(i)
                    Wort(i) = Wort(i + 1)
                    Wort(i + 1) = strWort
                    blogetauscht = True
                End If

            Next

        Loop Until blogetauscht = False

        Return Wort

    End Function

    '-----------------------------------------------------------------------
    ' RippelsortWortUmlaute Funktion für Zeichen
    ' Erklärung http://de.wikipedia.org/wiki/Shakersort
    '-----------------------------------------------------------------------

    Function RippelsortWortUmlaute(ByRef Wort() As String, ByVal Anfang As Integer, ByVal Ende As Integer)

        Dim i As Integer = Anfang
        Dim j As Integer = Ende
        Dim y As String

        For i = 1 To UBound(Wort)


            For j = (i + 1) To UBound(Wort)

                If Wort(i) > Wort(j) Then
                    y = Wort(i)
                    Wort(i) = Wort(j)
                    Wort(j) = y
                End If

            Next

        Next

        Return Wort

    End Function

End Module
