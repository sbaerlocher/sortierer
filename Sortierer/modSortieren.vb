﻿Module modSortieren

    '-----------------------------------------------------------------------
    ' QuickSort Funktion für Zeichen
    ' Erklärung http://de.wikipedia.org/wiki/Quicksort
    '-----------------------------------------------------------------------

    Function QuickSort(ByRef strSort As String, ByVal Anfang As Integer, ByVal Ende As Integer, ByRef boolUmlauteChecked As Boolean)

        Dim i As Integer = Anfang
        Dim j As Integer = Ende
        Dim X As String = Mid(strSort, (Math.Round((Anfang + Ende) / 2)), 1)
        Dim y As String

        Do
            If boolUmlauteChecked = True Then

                While Umlaut(Mid(strSort, i, 1)) < Umlaut(X)
                    i = i + 1
                End While

                While Umlaut(X) < Umlaut(Mid(strSort, j, 1))
                    j = j - 1
                End While

            Else

                While Mid(strSort, i, 1) < X
                    i = i + 1
                End While

                While X < Mid(strSort, j, 1)
                    j = j - 1
                End While

            End If

            If i <= j Then
                y = Mid(strSort, i, 1)
                Mid(strSort, i, 1) = Mid(strSort, j, 1)
                Mid(strSort, j, 1) = y
                i = i + 1
                j = j - 1
            End If

        Loop Until i > j

        If Anfang < j Then
            QuickSort(strSort, Anfang, j, boolUmlauteChecked)
        End If

        If i < Ende Then
            QuickSort(strSort, i, Ende, boolUmlauteChecked)
        End If

        Return strSort

    End Function

    '-----------------------------------------------------------------------
    ' Bubbelsort Funktion für Zeichen
    ' Erklärung http://de.wikipedia.org/wiki/Bubblesort
    '-----------------------------------------------------------------------

    Function BubbleSort(ByRef strSort As String, boolUmlauteChecked As Boolean)

        Dim chSort As Char
        Dim blogetauscht As Boolean

        Do
            blogetauscht = False

            For i = 1 To Len(strSort) - 1

                If boolUmlauteChecked = True Then

                    If Umlaut(Mid(strSort, i, 1)) > Umlaut(Mid(strSort, i + 1, 1)) Then
                        chSort = Mid(strSort, i, 1)
                        Mid(strSort, i, 1) = Mid(strSort, i + 1, 1)
                        Mid(strSort, i + 1, 1) = chSort
                        blogetauscht = True
                    End If

                Else

                    If Mid(strSort, i, 1) > Mid(strSort, i + 1, 1) Then
                        chSort = Mid(strSort, i, 1)
                        Mid(strSort, i, 1) = Mid(strSort, i + 1, 1)
                        Mid(strSort, i + 1, 1) = chSort
                        blogetauscht = True
                    End If

                End If

            Next

        Loop Until blogetauscht = False

        Return strSort

    End Function

    '-----------------------------------------------------------------------
    ' Rippelsort Funktion für Zeichen
    ' Erklärung http://de.wikipedia.org/wiki/Shakersort
    '-----------------------------------------------------------------------

    Function Rippelsort(ByRef strSort As String, ByVal Anfang As Integer, ByVal Ende As Integer, ByRef boolUmlauteChecked As Boolean)

        Dim i As Integer = Anfang
        Dim j As Integer = Ende
        Dim y As String

        For i = 1 To Len(strSort) - 1

            For j = (i + 1) To Len(strSort)

                If boolUmlauteChecked = True Then

                    If Umlaut(Mid(strSort, i, 1)) > Umlaut(Mid(strSort, j, 1)) Then
                        y = Mid(strSort, i, 1)
                        Mid(strSort, i, 1) = Mid(strSort, j, 1)
                        Mid(strSort, j, 1) = y
                    End If

                Else

                    If Mid(strSort, i, 1) > Mid(strSort, j, 1) Then
                        y = Mid(strSort, i, 1)
                        Mid(strSort, i, 1) = Mid(strSort, j, 1)
                        Mid(strSort, j, 1) = y
                    End If

                End If

            Next

        Next

        Return strSort

    End Function

    '-----------------------------------------------------------------------
    ' Funktion für das Auswechseln der Umlauten
    '-----------------------------------------------------------------------

    Public Function Umlaut(Zeichen As String)

        Select Case Zeichen
            Case "À", "Á", "Â", "Ã", "Ä", "Å"
                Umlaut = "A"
            Case "à", "á", "â", "ã", "ä"
                Umlaut = "a"
            Case " È", "É", "Ê", "Ë", "Ē"
                Umlaut = "E"
            Case "è", "é ", "ê", " ë ", "ē"
                Umlaut = "e"
            Case "Ì", "I", "Î", "Ï"
                Umlaut = "I"
            Case "ì", "í", "î", "ï"
                Umlaut = "i"
            Case "  Ò", "Ó", "Ô", "Õ", "Ö"
                Umlaut = "O"
            Case "ò", "ó", "ô", "õ", "ö"
                Umlaut = "o"
            Case "Ù", "Ú", "Û", "Ü"
                Umlaut = "U"
            Case "ù", "ú", "û", "ü"
                Umlaut = "u"
            Case Else
                Umlaut = Zeichen
        End Select

        Return Umlaut

    End Function

End Module
